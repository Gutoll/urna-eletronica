/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author vrglh
 * Classe Singleton
 */
class BancoDeDados implements java.io.Serializable {
    //Singleton; Thread-safe (porque atributos estáticos criados quando declarados
    //são garantidos de serem criados na primeira vez que são acessados)
    public final static BancoDeDados INSTANCE = new BancoDeDados();
    private final Map<Integer, Candidato> ListaCandidatos = new ConcurrentHashMap<>();
    
    private BancoDeDados() {
        /*** Debug ***/
        Candidato bolsomito = new Candidato(1120, "Jair Bolsomito", "PP");
        Candidato jeanCuspyllys = new Candidato(5005, "Jean Cuspyllys", "PSOL");
        ListaCandidatos.put(1120, bolsomito);
        ListaCandidatos.put(5005, jeanCuspyllys);
        /*************/
        //Existe para previnir inicialização
    }

    protected Map<Integer, Candidato> getListaCandidatos() {
        return ListaCandidatos;
    }
    
    protected void updateVotos(Collection<Candidato> Candidatos) {
        Candidatos.stream().forEach((candidato) -> {
            ListaCandidatos.get(candidato.getCodigo_votacao()).incrNum_votos(candidato.getNum_votos());
        });
    }
    
    //Para a serialização
    private Object readResolve() {
            return INSTANCE;
    }
}
