/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vrglh
 */
public class ConnectionHandler implements Runnable {

    @Override
    public void run() {
        try {
            run_throws();
        } catch (IOException ex) {
            Logger.getLogger(ConnectionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void run_throws() throws IOException { //Porque run nao lança IOException
        ServerSocket server = new ServerSocket(Constants.PORT);
        BancoDeDados bd = BancoDeDados.INSTANCE;
        
        while (true) {
            System.out.println("Aguardando conexoes...");
            Socket socket = server.accept();
            System.out.println("Conectado!");

            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());

            try {
                Integer opcode = (Integer)input.readObject();
                
                switch (opcode) {
                    case Constants.OPCODE_LISTA:
                        HashMap<Integer, Candidato> ListaCandidatos = new HashMap<>(bd.getListaCandidatos());
                        ListaCandidatos.values().stream().forEach((candidato) -> {
                            candidato.setNum_votos(0);
                        });
                        output.writeObject(ListaCandidatos.values());
                        break;
                    case Constants.OPCODE_VOTOS:
                        bd.updateVotos((Collection<Candidato>) input.readObject());
                        break;
                    default:
                        //Força fechamento dos Streams
                        throw new IOException();
                }
            } catch (ClassNotFoundException|IOException ex) {
                try {
                    input.close();
                    output.close();
                } catch (IOException ex1) {}
            }
               
        }
    }
    
}
