/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

/**
 *
 * @author vrglh
 */
public final class Constants {
    public final static int PORT = 10042;
    public final static int OPCODE_LISTA = 999;
    public final static int OPCODE_VOTOS = 888;
    
    private Constants() {
        //Previne inicialização
    }
}
